package ulr.l3i.strdfmining;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import weka.associations.Apriori;
import weka.core.Instances;
import weka.core.Instance;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.associations.AprioriItemSet;
import java.text.DecimalFormat;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/*Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à "STRDFMining". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to STRDFMining (Spatio-Temporal RDF Mining).

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/
/**
 * AprioriWeb
 * @author Ba-Huy Tran, Alain Bouju
 */
public class AprioriWeb {

    static Instances dataOld; // before filtering
    static Instances data; // after filtering
    static Apriori apriori;
    static String filename;
    static int size;

    /**
     * RunApriori
     * @param fn file name
     * @param variable
     * @param supp
     * @param conf
     * @throws IOException
     * @throws Exception
     */
    public static void RunApriori(String fn, String variable, float supp, float conf) throws IOException, Exception {
        CSVLoader loader = new CSVLoader();
        loader.setFile(new File(fn));
        dataOld = loader.getDataSet();
        Remove remove = new Remove();
        variable = variable.replaceAll("-", ",");
        remove.setAttributeIndices(variable);
        remove.setInvertSelection(true);
        remove.setInputFormat(dataOld);
        data = Filter.useFilter(dataOld, remove);
        size = data.numAttributes();
        apriori = new Apriori();
        for (int i = 0; i < size; i++) {
            data.renameAttribute(data.attribute(i), "?" + data.attribute(i).name());
        }
        apriori.setLowerBoundMinSupport(supp);
        apriori.setMinMetric(conf);
        apriori.setNumRules(400);
        apriori.buildAssociations(data);
        filename = fn;
    }

    /**
     * listRules
     * @return html
     * @throws UnsupportedEncodingException
     */
    public static String listRules() throws UnsupportedEncodingException {
        String s = "<b>Association rules found</b><br>";
        int n = 0;
        for (int i = 0; i < apriori.getAllTheRules()[0].size(); i++) {
            AprioriItemSet condition = ((AprioriItemSet) apriori.getAllTheRules()[0].elementAt(i));
            AprioriItemSet consequence = ((AprioriItemSet) apriori.getAllTheRules()[1].elementAt(i));
            double confidence = ((Double) apriori.getAllTheRules()[2].elementAt(i)).doubleValue();
            DecimalFormat df = new DecimalFormat("#.##");
            // if(confidence<1)
            // {
            String cond = condition.toString(data);
            String cons = consequence.toString(data);
            if (cond.split("=").length == (size)) {
                n++;
                String p = new String(cond.substring(0, cond.lastIndexOf(" ")).replaceAll(" \\?", " && \\?").getBytes(), "UTF-8");
                String c = new String(cons.substring(0, cons.lastIndexOf(" ")).getBytes(), "UTF-8");
                // s+="<a href='javascript:show(\"filter("+ p+" && "+c+")\")'>"+n+". "+p+" => "+c+ " ("+df.format(confidence)+") </a><br>";
                s += "<a href='javascript:showrule(" + i + ")'>" + n + ". " + p + " => " + c + " (" + df.format(confidence) + ") </a><br>";
            }

        }
        s += "<br><b> " + n + " rules listed/ " + apriori.getAllTheRules()[0].size() + " rules found </b>";
        return s;
    }

    /**
     * getRuleInstances 
     * @param n number
     * @return html
     * @throws UnsupportedEncodingException
     */
    public static String getRuleInstances(int n) throws UnsupportedEncodingException {
        int point = 0;
        int poly = 0;
        int c = 0;
        String s = "<table id='list' class='list'><th>N0</th>";
        String ss = "";
        //print Header
        for (int j = 0; j < dataOld.numAttributes(); j++) {
            s += "<th>" + dataOld.attribute(j).name() + "</th>";
        }

        //print Instances
        AprioriItemSet condition = ((AprioriItemSet) apriori.getAllTheRules()[0].elementAt(n));
        AprioriItemSet consequence = ((AprioriItemSet) apriori.getAllTheRules()[1].elementAt(n));
        Instance ins, ins2;
        int m = 1, k = 1;
        for (int i = 0; i < dataOld.numInstances(); i++) {
            ins = data.instance(i);
            ins2 = dataOld.instance(i);
            if (condition.containedBy(ins) && consequence.containedBy(ins)) {
                point = 0;
                poly = 0;
                s += "<tr><td>" + m + "</td>";
                m++;
                for (int j = 0; j < ins2.numAttributes(); j++) {
                    String data = ins2.toString(j).replace("'", "");
                    if (data.contains("POLY")) {
                        s += "<td class='geomvalue'><a id='poly" + poly + i + "' href='javascript:visual(\"poly" + poly + "\")'>" + StringEscapeUtils.escapeHtml(data) + "</a></td>";
                        poly = 1;
                    } else if (data.contains("POINT")) {
                        s += "<td class='geomvalue' ><a id='point" + point + i + "' href='javascript:visual(\"point" + point + "\")'>" + StringEscapeUtils.escapeHtml(data) + "</a></td>";
                        point = 1;
                    } else {
                        s += "<td><a href='#'>" + new String(data.getBytes(), "UTF-8") + "</a></td>";
                    }
                }
                s += "</tr>\n";
            } else if (condition.containedBy(ins) && !consequence.containedBy(ins)) {
                point = 0;
                poly = 0;
                ss += "<tr><td>" + k + "</td>";
                k++;
                for (int j = 0; j < ins2.numAttributes(); j++) {
                    String data = ins2.toString(j).replace("'", "");
                    if (data.contains("POLY")) {
                        ss += "<td class='geomvalue'><a id='poly" + poly + i + "' href='javascript:visual(\"poly" + poly + "\")'>" + StringEscapeUtils.escapeHtml(data) + "</a></td>";
                        poly = 1;
                    } else if (data.contains("POINT")) {
                        ss += "<td class='geomvalue' ><a id='point" + point + i + "' href='javascript:visual(\"point" + point + "\")'>" + StringEscapeUtils.escapeHtml(data) + "</a></td>";
                        point = 1;
                    } else {
                        ss += "<td><a href='#'>" + new String(data.getBytes(), "UTF-8") + "</a></td>";
                    }
                }
                ss += "</tr>\n";
            }

        }
        s += "<tr><td colspan=></td></tr>" + ss + "</table>";
        return s;
    }

    /**
     * getRuleInstancesJSON
     * @param n number
     * @return JSON
     * @throws UnsupportedEncodingException
     */
    public static String getRuleInstancesJSON(int n) throws UnsupportedEncodingException {
        JSONArray json = new JSONArray();
        AprioriItemSet condition = ((AprioriItemSet) apriori.getAllTheRules()[0].elementAt(n));
        AprioriItemSet consequence = ((AprioriItemSet) apriori.getAllTheRules()[1].elementAt(n));
        Instance ins, ins2;

        for (int i = 0; i < dataOld.numInstances(); i++) {
            ins = data.instance(i);
            ins2 = dataOld.instance(i);

            org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
            //if contain instance
            if (condition.containedBy(ins) && consequence.containedBy(ins)) {
                for (int j = 0; j < ins2.numAttributes(); j++) {
                    obj.put(dataOld.attribute(j).name(), ins2.toString(j));
                }
                json.add(obj);
            }
        }
        return json.toJSONString();
    }
}

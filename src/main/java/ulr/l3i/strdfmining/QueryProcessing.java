package ulr.l3i.strdfmining;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.List;

import org.apache.commons.codec.binary.Hex;

import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.query.QuerySolution;

import org.apache.jena.update.UpdateRequest;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateExecutionFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


/*Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à "STRDFMining". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to STRDFMining (Spatio-Temporal RDF Mining).

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * QueryProcessing
 * 
 * @author Ba-Huy Tran, Alain Bouju
 */

public class QueryProcessing {

	static String path = Config.getJSPPath()+"/KB/Cache/"; /*/gemcache/";*/

	/**
	 * update
	 * @param req request
	 */
	public static void update(String req) {
		UpdateRequest re = UpdateFactory.create(req);
		String sparqlEndPoint= null;
		switch (Config.triplestore) {
		case "Marmotta":
			sparqlEndPoint= Config.endpoint;
			break;
		case "Strabon":
			sparqlEndPoint= Config.endpoint+"/Update";
			break;
		default:
			throw new IllegalArgumentException("Invalid triplestore: " + Config.triplestore);
		}
		System.out.println("sparqlEndpoint "+sparqlEndPoint);
		UpdateProcessor up = UpdateExecutionFactory.createRemote(re, sparqlEndPoint);
		up.execute();

	}

	/**
	 * querydirect
	 * @param req request
	 * @return result
	 */
	public static String querydirect(String req) {
		System.out.println("Start requete querydirect "+req);
		String sparqlEndPoint= null;
		switch (Config.triplestore) {
		case "Marmotta":
			sparqlEndPoint= Config.endpoint;
			break;
		case "Strabon":
			sparqlEndPoint= Config.endpoint+"/Query";
			break;
		default:
			throw new IllegalArgumentException("Invalid triplestore: " + Config.triplestore);
		}
		System.out.println("sparqlEndpoint "+sparqlEndPoint);
		Query query = QueryFactory.create(req);
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlEndPoint, query);
		ResultSet rs1 = qe.execSelect();
		rs1 = ResultSetFactory.copyResults(rs1) ;
		// Close query execution
		qe.close();
		return ResultSetFormatter.asText(rs1);

	}

	/**
	 * queryCSV
	 * @param req request
	 * @return result as CSV
	 */
	public static String queryCSV(String req) {
		System.out.println("Start requete queryCSV "+req);
		String sparqlEndPoint= null;
		switch (Config.triplestore) {
		case "Marmotta":
			sparqlEndPoint= Config.endpoint;
			break;
		case "Strabon":
			sparqlEndPoint= Config.endpoint+"/Query";
			break;
		default:
			throw new IllegalArgumentException("Invalid triplestore: " + Config.triplestore);
		}		
		System.out.println("sparqlEndpoint "+sparqlEndPoint);
		Query query = QueryFactory.create(req);
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlEndPoint, query);/* strabon + "/Query"*/
		ResultSet rs1 = qe.execSelect();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ResultSetFormatter.outputAsCSV(os, rs1);
		// Close query execution
		qe.close();
		return new String(os.toByteArray());
	}

	
	/**
	 * queryCSVQ
	 * @param req request
	 * @return result
	 */
	public static String queryCSVQ(String req) {
		System.out.println("Start requete queryCSVQ "+req);
		String sparqlEndPoint= null;
		switch (Config.triplestore) {
		case "Marmotta":
			sparqlEndPoint= Config.endpoint;
			break;
		case "Strabon":
			sparqlEndPoint= Config.endpoint+"/Query";
			break;
		default:
			throw new IllegalArgumentException("Invalid triplestore: " + Config.triplestore);
		}
		System.out.println("sparqlEndpoint "+sparqlEndPoint);
		System.out.println("req "+req);

		//String s = "";
		Query query = QueryFactory.create(req);
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlEndPoint, query);/* strabon + "/Query"*/
		ResultSet rs = null;
		try {
		 rs = qe.execSelect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<String> var = rs.getResultVars();
		StringBuilder s= new StringBuilder();

		for (int i = 0; i < var.size(); i++) {
			s.append("," + var.get(i));
		}
		s.delete(0,1);
		s.append("\n");

		while (rs.hasNext()) {

			QuerySolution qs = rs.next();
			String sss = "";
			for (int i = 0; i < var.size(); i++) //iterate columns
			{
				String column_name = var.get(i);
				String ss = qs.get(column_name).toString();
				if (ss.indexOf("^") > 0) {
					ss = ss.substring(0, ss.indexOf("^"));
				}
				ss = ss.replaceAll("'", "_");
				//   if(ss.contains(",")) 
				//        ss="\""+ss+"\"";
				sss = sss + ",\"" + ss+"\"";

			}
			s.append(sss.substring(1) + "\n");
		}
		// Close query execution
		qe.close();
		return s.toString();
	}

	/**
	 * queryJSON
	 * @param req request
	 * @return result as JSON
	 */
	public static String queryJSON(String req) {
		System.out.println("Start requete queryJSON "+req);
		String sparqlEndPoint= null;
		Config.getEndpointURL();
		System.out.println("Config.triplestore "+Config.triplestore);

		switch (Config.triplestore) {
		case "Marmotta":
			sparqlEndPoint= Config.endpoint;
			break;
		case "Strabon":
			sparqlEndPoint= Config.endpoint+"/Query";
			break;
		default:
			System.out.println("Default");
			throw new IllegalArgumentException("Invalid triplestore: " + Config.triplestore);
		}
		System.out.println("sparqlEndpoint "+sparqlEndPoint);
		System.out.println("req "+req);

		Query query = QueryFactory.create(req);
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlEndPoint, query);
		ResultSet rs = qe.execSelect();
		
		System.out.println("End requete "+rs.getRowNumber());

		List<String> var = rs.getResultVars();
		JSONArray json = new JSONArray();
		while (rs.hasNext()) {
			JSONObject obj = new JSONObject();
			QuerySolution qs = rs.next();
			for (int i = 0; i < var.size(); i++) //iterate columns
			{
				String column_name = var.get(i);
				String ss = qs.get(column_name).toString();
				if (ss.indexOf("^") > 0) {
					ss = ss.substring(0, ss.indexOf("^"));
				}

				//  if (isNumeric(qs.get(column_name).toString())) {
				obj.put(column_name, ss);
				//  } else {
				//       obj.put(column_name, resultSet.getObject(column_name).toString());
				//  }
			}
			json.add(obj);
		}
		// Close query execution
		qe.close();
		System.out.println("End queryJSON");
		return json.toJSONString();
	}

	/**
	 * query
	 * @param req request
	 * @param filePath result file as CSV
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void query(String req, String filePath) throws FileNotFoundException, IOException {
		// filePath="c:/down/rs.csv";
		System.out.println("Start requete query "+req);
		String sparqlEndPoint= null;
		switch (Config.triplestore) {
		case "Marmotta":
			sparqlEndPoint= Config.endpoint;
			break;
		case "Strabon":
			sparqlEndPoint= Config.endpoint+"/Query";
			break;
		default:
			throw new IllegalArgumentException("Invalid triplestore: " + Config.triplestore);
		}
		System.out.println("sparqlEndpoint "+sparqlEndPoint);
		Query query = QueryFactory.create(req);
		String s = "";
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlEndPoint, query);
		ResultSet rs1 = qe.execSelect();
		ResultSetFormatter.outputAsCSV(new FileOutputStream(new File(filePath)), rs1);
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		BufferedWriter bw = new BufferedWriter(new FileWriter(filePath + "p"));
		String ss;
		while ((ss = br.readLine()) != null) {
			ss = ss.replaceAll(";http://www.opengis.net/def/crs/EPSG/0/4326", "");
			ss = ss.replaceAll("\"\"\"", "");
			ss = ss.replaceAll("^^<http://www.w3.org/2001/XMLSchema#double>", "");
			//ss = new String(ss.getBytes(), "UTF-8"); 
			//ss = Normalizer.normalize(ss, Normalizer.Form.NFD);
			// ss = ss.replaceAll("\\p{M}", "");
			//   ss=ss.replaceAll(" ", "");
			ss = ss.replaceAll("'", "");
			bw.write(ss + "\n");
		}
		bw.close();
		br.close();
		// Close query execution
		qe.close();
	}

	
	/**
	 * queryToFile
	 * @param req request
	 * @return file name
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public static String queryToFile(String req) throws FileNotFoundException, IOException, NoSuchAlgorithmException {
		// filePath="c:/down/rs.csv";
		System.out.println("Start requete queryToFile "+req);
		String sparqlEndPoint = null;
		switch (Config.triplestore) {
		case "Marmotta":
			sparqlEndPoint= Config.endpoint;
			break;
		case "Strabon":
			sparqlEndPoint= Config.endpoint+"/Query";
			break;
		default:
			throw new IllegalArgumentException("Invalid triplestore: " + Config.triplestore);
		}
		System.out.println("sparqlEndpoint "+sparqlEndPoint);
		String fn = path + getMD5(req + "csv");
		if (new File(fn).exists()) {
			return fn;
		}
		Query query = QueryFactory.create(req);
		String s = "";
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlEndPoint, query);
		ResultSet rs1 = qe.execSelect();
		ResultSetFormatter.outputAsCSV(new FileOutputStream(new File(fn + ".temp")), rs1);
		BufferedReader br = new BufferedReader(new FileReader(fn + ".temp"));
		BufferedWriter bw = new BufferedWriter(new FileWriter(fn));
		String ss;
		while ((ss = br.readLine()) != null) {
			ss = ss.replaceAll(";http://www.opengis.net/def/crs/EPSG/0/4326", "");
			ss = ss.replaceAll("\"\"\"", "");
			ss = ss.replaceAll("^^<http://www.w3.org/2001/XMLSchema#double>", "");
			//ss = new String(ss.getBytes(), "UTF-8"); 
			//ss = Normalizer.normalize(ss, Normalizer.Form.NFD);
			// ss = ss.replaceAll("\\p{M}", "");
			//   ss=ss.replaceAll(" ", "");
			ss = ss.replaceAll("'", "");
			bw.write(ss + "\n");
		}
		bw.close();
		br.close();
		File file = new File(fn + ".temp");
		file.delete();
		// Close query execution
		qe.close();
		return fn;
	}

	/**
	 * getMD5
	 * @param req request
	 * @return MD5 hash key
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public static String getMD5(String req) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String req2 = req.toLowerCase();
		String hash = req2.substring(req2.indexOf("select"));
		hash = hash.replaceAll("\\s+", "");
		hash = hash.replaceAll("\\r|\\n", "");
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] md5 = md.digest(hash.trim().getBytes("UTF-8"));
		return new String(Hex.encodeHex(md5));
	}

}

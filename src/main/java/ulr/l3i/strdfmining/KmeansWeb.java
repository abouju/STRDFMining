/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ulr.l3i.strdfmining;


import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.simple.JSONArray;
import weka.clusterers.SimpleKMeans;

import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/*Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à "STRDFMining". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to STRDFMining (Spatio-Temporal RDF Mining).

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/
/**
 * KmeansWeb
 * @author Ba-Huy Tran, Alain Bouju
 */
public class KmeansWeb {

    static Instances dataOld;
    static SimpleKMeans kMeans;
    static String filename;

    /**
     * RunKmean
     * @param fn file name
     * @param variable 
     * @param num number of cluster
     * @throws IOException
     * @throws Exception
     */
    public static void RunKmean(String fn, String variable, int num) throws IOException, Exception {
        CSVLoader loader = new CSVLoader();
        loader.setFile(new File(fn));
        dataOld = loader.getDataSet();
        Remove remove = new Remove();
        variable = variable.replaceAll("-", ",");
        remove.setAttributeIndices(variable);
        remove.setInvertSelection(true);
        remove.setInputFormat(dataOld);
        Instances data = Filter.useFilter(dataOld, remove);
        kMeans = new SimpleKMeans();
        kMeans.setNumClusters(num);
        kMeans.setPreserveInstancesOrder(true);
        kMeans.buildClusterer(data);
        filename = fn;
    }

    /**
     * getCentroids
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getCentroids() throws UnsupportedEncodingException {
        String s = "";
        Instances centroids = kMeans.getClusterCentroids();
        for (int i = 0; i < centroids.numInstances(); i++) {
            s += "<a href='javascript:showcluster(" + i + ")'> Centroid " + i + ": " + new String(centroids.instance(i).toString().getBytes(), "UTF-8") + " [" + kMeans.getClusterSizes()[i] + "]</a><br>";
        }
        return s;
    }

    /**
     * getAssignments
     * @param cluster
     * @return html table
     * @throws Exception
     */
    public static String getAssignments(int cluster) throws Exception {
        int point = 0;
        int poly = 0;
        int c = 0;
        String s = "<table id='list' class='list'><th>N0</th>";
        //print Header
        for (int j = 0; j < dataOld.numAttributes(); j++) {
            s += "<th>" + dataOld.attribute(j).name() + "</th>";
        }

        //print Instances
        int[] assignments = kMeans.getAssignments();
        for (int i = 0; i < assignments.length; i++) {
            if (assignments[i] == cluster) {
                point = 0;
                poly = 0;
                s += "<tr><td>" + i + "</td>";
                for (int j = 0; j < dataOld.instance(i).numAttributes(); j++) {
                    String data = dataOld.instance(i).toString(j).replace("'", "");
                    if (data.contains("POLY")) {
                        s += "<td class='geomvalue'><a id='poly" + poly + i + "' href='javascript:visual(\"poly" + poly + "\")'>" + StringEscapeUtils.escapeHtml(data) + "</a></td>";
                        poly = 1;
                    } else if (data.contains("POINT")) {
                        s += "<td class='geomvalue' ><a id='point" + point + i + "' href='javascript:visual(\"point" + point + "\")'>" + StringEscapeUtils.escapeHtml(data) + "</a></td>";
                        point = 1;
                    } else {
                        s += "<td><a href='#'>" + new String(data.getBytes(), "UTF-8") + "</a></td>";
                    }
                }
                s += "</tr>\n";
            }
        }

        s += "</table>";
        return s;
    }

    /**
     * getAssignmentsJSON
     * @param cluster
     * @return JSON
     * @throws Exception
     */
    public static String getAssignmentsJSON(int cluster) throws Exception {
        JSONArray json = new JSONArray();
    
        int[] assignments = kMeans.getAssignments();
        for (int i = 0; i < assignments.length; i++) {
            if (assignments[i] == cluster) {
                org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
                for (int j = 0; j < dataOld.instance(i).numAttributes(); j++) {
                    //   String data = dataOld.instance(i).toString(j).replace("'", "");
                    obj.put(dataOld.attribute(j).name(), dataOld.instance(i).toString(j));

                }
                json.add(obj);
            }

        }
        return json.toJSONString();

    }
}

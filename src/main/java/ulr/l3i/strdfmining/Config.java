/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ulr.l3i.strdfmining;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
/*Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à "STRDFMining". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to STRDFMining (Spatio-Temporal RDF Mining).

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/
/**
 * Config
 * @author Ba-Huy Tran, Alain Bouju
 */
public  class Config {
	//http://localhost:8080/marmotta/sparql/select/
	//http://localhost:8080/strabon-endpoint-3.2.12-temporals-SNAPSHOT/
	public static boolean init=false;
    public static String endpoint=null;
    public static String triplestore=null;
    public static String jspPath=null;
    
    
    /**
     * setJSPPath set application path
     * @param pJSPPath
     */
    public static void setJSPPath(String pJSPPath)  
    {
    	jspPath = pJSPPath;
    }
    
    /**
     * @return application path
     */
    public static String getJSPPath()  
    {
    	return jspPath;
    }
    
    /**
     * setEnpoint set triplestore type and url
     * @param pTriplestore
     * @param pEndpoint
     */
    public static void setEnpoint(String pTriplestore,String pEndpoint)
    {
    	try {
			String propFileName = jspPath+"/config/endpoint.properties";
			endpoint = pEndpoint;
			pTriplestore = pTriplestore;
            Properties props = new Properties();
            props.setProperty("endpoint", pEndpoint);
            props.setProperty("triplestore", pTriplestore);
            File f = new File(propFileName);
            OutputStream out = new FileOutputStream( f );
            props.store(out, "Config endpoint type and url");
            init=true;
        }
        catch (Exception e ) {
            e.printStackTrace();
        }
    }
    
    /**
     * getEndpointURL read endpoint config from endpoint.properties
     */
    public static void getEndpointURL()
    {
    	// lecture d'un modele de fiche
    			Properties prop = new Properties();
    			String propFileName = jspPath+"/config/endpoint.properties";

    			// Lecture property
    			FileInputStream inputStream=null;
    			try {
    				inputStream = new FileInputStream(propFileName);
    			} catch (FileNotFoundException e) {
    				e.printStackTrace();
    			}

    			try{
    				prop.load(inputStream);
    				if (inputStream == null) {
    					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
    				}
    			} catch (Exception ex)
    			{
    				ex.printStackTrace();
    			}
    			endpoint = prop.getProperty("endpoint");
    			triplestore = prop.getProperty("triplestore");
    
   }
}

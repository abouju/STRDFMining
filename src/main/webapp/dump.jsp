<%-- 
    Document   : dump
    Created on : Sep 2, 2017, 9:56:43 AM
    Author     : Ba Huy Tran, Alain Bouju


Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à [rappeler les
caractéristiques techniques de votre logiciel]. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
--%>
 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <style>
            #container
            {
                height: 800px;
            }
            #div_right
            {
                margin-left: 20px;
                float:left;
            
                height: 750px;
                width: 0px;
            }
            #div_left
            {
                float:left;
                width: 1000px;
                overflow: auto;
            }
            #mapping,#result
            {
                height: 90%;
                width:95%;
                overflow: scroll;
                font-size: larger;             
            }

            #fn
            {
                float:left; 
            }

        </style>
        <script>
            $(document).ajaxStart(function () {
                $(document.body).css({'cursor': 'wait'});
            }).ajaxStop(function () {
                $(document.body).css({'cursor': 'default'});
            });
        </script>
    </head>
    <body>
        <div id='container'>
            <div id='div_left'>
                <textarea rows="40" id="mapping" name='mapping'></textarea><br>
                <input type='text' id='fn' name='fn' placeholder="Dump file name" size="40"> 
                <input type="checkbox" id="view"> View            <button class="ui-button ui-widget ui-corner-all" id='dump'> Dump </button>
            </div>
            <div id='div_right'>
                <textarea rows="40" id="result"></textarea><br>

            </div>
        </div>
        <script>

            $("#dump").click(function () {
                $.get("rundump.jsp",
                        {
                            fn: $("#fn").val(),
                            mapping: $("#mapping").val(),
                            view: $('#view').is(':checked')
                        },
                        function (data, status) {
                            if ($('#view').is(':checked') === true)
                            {
                              
                                $('#result').html(data);
                                $('#div_left').width(500);
                                $('#div_right').width(1200);
                            } else
                            {
                                if (data.trim() === "OK")
                                    alert("OK");
                                $('#div_right').width(0);
                                $('#div_left').width(1000);
                            }
                        });
            });
        </script>
</html>

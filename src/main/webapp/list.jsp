<%-- 
    Document   : list
    Created on : Nov 25, 2015, 5:18:02 PM
    Author     : Ba Huy Tran, Alain Bouju


Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à "STRDFMining". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to STRDFMining (Spatio-Temporal RDF Mining).

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
--%>

<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="ulr.l3i.strdfmining.AprioriWeb"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="org.apache.commons.codec.binary.Hex"%>
<%@page import="java.security.MessageDigest"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="ulr.l3i.strdfmining.QueryProcessing"%>
<%@page import="ulr.l3i.strdfmining.Config"%>
<%@page import="java.nio.charset.Charset"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="org.json.simple.parser.ParseException"%>
<%@page import="java.util.Iterator"%>

<%
	// Config endpoint
	Config.setJSPPath(request.getRealPath("/"));
	Config.getEndpointURL();
	// Sparql request
	//String req = StringEscapeUtils.unescapeJavaScript(request.getParameter("q"));
	// URLEnncoding and èé...
	String req = java.net.URLDecoder.decode(((String[]) request.getParameterMap().get("q"))[0], "ISO-8859-1");
	String format = request.getParameter("f").toLowerCase();
	String algo = StringEscapeUtils.unescapeJavaScript(request.getParameter("algo"));
	//String param = request.getParameter("param").toLowerCase();
	String param = request.getParameter("param");
	//String var = request.getParameter("variables").toLowerCase();
	String var = request.getParameter("variables");

	if (var == null) {
		var = new String("false");
	}

	System.out.println("algo " + algo + " format " + format + " requete " + req + " variables " + var);

	if (algo.equals("none")) {
		//compute cache Key 
		String result = QueryProcessing.getMD5(req + format);
		File file = new File(request.getRealPath("/") + "/KB/Cache/" + result);
		// if cache exist
		if (file.exists()) {
			//  result variable
			if (var.equals("true")) {
				System.out.println("list.jsp first var ");
				String resultVar = new String();
				JSONParser parser = new JSONParser();
				try {
					JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(file));
					if (jsonArray.size() > 0) {
						// First Line
						System.out.println("First: " + jsonArray.get(0));
						JSONObject line = (JSONObject) jsonArray.get(0);
						Iterator<String> i = line.keySet().iterator();
						if (i.hasNext()) {
							resultVar = i.next();
						}
						while (i.hasNext()) {
							resultVar = resultVar + " " + i.next();
							System.out.println(resultVar);
						}
					} else {
						resultVar = resultVar + "?error";
					}
					out.println(resultVar);
					out.flush();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				String line = "", l = "";
				BufferedReader br = new BufferedReader(new InputStreamReader(
						new FileInputStream(request.getRealPath("/") + "/KB/Cache/" + result)));
				while ((l = br.readLine()) != null) {
					line += l + "\n";
				}
				out.write(line);
			}
		} // not file exist
		else {
			String resp = "";
			if (format.equals("json")) {
				System.out.println("json " + req);

				resp = QueryProcessing.queryJSON(req);

				// if not variable stream
				if (!var.equals("true")) {
					out.write(resp);
				}

			} else if (format.equals("csv")) {
				System.out.println("csv" + req);

				resp = QueryProcessing.queryCSVQ(req);
				out.write(resp);

			}
			// Write cache
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(resp);
			bw.close();
			if (var.equals("true")) {
				System.out.println("list.jsp first var ");
				String resultVar = new String();
				JSONParser parser = new JSONParser();
				try {
					JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(file));
					if (jsonArray.size() > 0) {
						// First Line
						System.out.println("First: " + jsonArray.get(0));
						JSONObject line = (JSONObject) jsonArray.get(0);
						Iterator<String> i = line.keySet().iterator();
						if (i.hasNext()) {
							resultVar = i.next();
						}
						while (i.hasNext()) {
							resultVar = resultVar + " " + i.next();
							System.out.println(resultVar);
						}
					} else {
						resultVar = resultVar + "?error";
					}
					out.println(resultVar);
					out.flush();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
	} else if (algo.equals("apriori")) {
		String s = AprioriWeb.getRuleInstancesJSON(Integer.parseInt(param));
		out.print(s.replaceAll("'", ""));

	}

	System.out.println("End List");
%>
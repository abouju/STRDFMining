<%-- 
    Document   : index
    Created on : Nov 25, 2015, 5:18:02 PM
    Author     : Ba Huy Tran, Alain Bouju

 
Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à "STRDFMining". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to STRDFMining (Spatio-Temporal RDF Mining).

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
--%>
 

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="ulr.l3i.strdfmining.Config"%>
<%@page import="javax.servlet.ServletContext"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>STRDFMining</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!--  SPARQL EDITOR -->
        <link href='//cdn.jsdelivr.net/npm/yasgui-yasqe@2.11.22/dist/yasqe.min.css' rel='stylesheet' type='text/css'/>
        <script src='//cdn.jsdelivr.net/npm/yasgui-yasqe@2.11.22/dist/yasqe.bundled.min.js'></script>
        <script type="text/javascript" src="tabulator.js"></script>  
        <link href="tabulator.css" rel="stylesheet">
        <link href="RDFMining.css" rel="stylesheet">
        <script>
            // init the dialog
            $(function () {
                $("#algo").selectmenu({
                    change: function (event, ui)
                    {
                        $('#kmean,#part,#apriori,#div_variable,#jrip').hide();
                        switch ($(this).val() * 1)
                        {
                            case 1:
                                $("#apriori").show();
                                $("#div_variable").show();
                                break;
                            case 2:
                                $("#part").show();
                                $("#div_variable").show();
                                break;
                            case 3:
                                $("#kmean").show();
                                $("#div_variable").show();
                                break;
                            case 4:
                                $("#jrip").show();
                                $("#div_variable").show();
                                break;
                        }
                    }
                });
                $('#kmean,#part,#apriori,#div_variable,#jrip').hide();
                $('input').addClass("ui-corner-all ui-widget");
                $('div').addClass("ui-corner-all");
            });

            // Set cursor
            $(document).ajaxStart(function () {
            	console.log("ajaxStart");
                $(document.body).css({'cursor': 'wait'});
            }).ajaxStop(function () {
            	console.log("ajaxStop");
                $(document.body).css({'cursor': 'default'});
            });
            

        </script>
    </head>
    <body>
       <%
         out.write("<input type='hidden' id='querypath' value='" + getServletContext().getRealPath("/KB/Query") + "'>");
         out.write("<input type='hidden' id='cachepath' value='" + getServletContext().getRealPath("/KB/Cache") + "'>");
       %>
       <script>

         var rq = "";
         var oldColumn = [];
         var algo = "none";
         var param = 0;
         var tableinit = 0;
         var cached = false;
         var va = []; // result variable 


	      // Show Table result from jsonlink
          function makeTable(ptableinit,jsonlink)
            {
        	  console.log("makeTable "+ptableinit);
	          // create
	          if (ptableinit === 0)
	            {
	        	   console.log("makeTable Init");
                   $("#div_list").tabulator({
	    	       fitColumns: true,
                   height: "100%",
                  });
	            } 
	          else // clean 
	            {
	        	   console.log("makeTable clean");

                   $("#div_list").tabulator("clearData");
                   $.each(oldColumn, function (key, value) {
                   $("#div_list").tabulator("deleteColumn", value.trim());
                   });
                   $("#div_list").tabulator("deleteColumn", "N0");
	            }


			// variable start with $ or ?
            oldColumn = va;
			
			// Create column
            $("#div_list").tabulator("addColumn", {title: "N0", field: "N0", formatter: "rownum", width: 80});
            $.each(va, function (key, value) {
               $("#div_list").tabulator("addColumn", {title: value.trim(), field: value.trim(), cellDblClick: function (e, cell) {
            	   window.open("RDFD.jsp?query=" + encodeURIComponent(cell.getValue()), '', 'height=800,width=1500');
               }});
            });
            
      	    //Fill data
            $("#div_list").tabulator("setData", jsonlink);
          }//function make table

          // Show the result in the bottom
          function show(s)
            {
              s = s.replace(/=/g, "=\"");
              s = s.replace(/ &&/g, "\" &&");
              s = s.replace(")", "\")");
              if (s.includes("http"))
                {
                  s = s.replace(/"h/g, "<h");
                  s = s.replace(/"/g, ">");
                }
              rq2 = rq.replace("}", s + " }");
              linklist = 'list.jsp?f=json&q=' + encodeURIComponent(rq2.replace(/\n/g, " "));
              $("#div_list").load(linklist);
            }

           function showrule(r)
             {
               makeTable(tableinit,"apriori.jsp?q=&r=" + r);
		 	   if (tableinit===0) tableinit=1;
               algo = "apriori";
               param = r;
             }
        
           function showJRip(n)
             {
               makeTable(tableinit,"jrip.jsp?q=&n=" + n);
               if (tableinit===0) tableinit=1;
               algo = "jrip";
               param = n;
             }

           function showcluster(c)
             {
               makeTable(tableinit,"kmeans.jsp?q=&c=" + c);
               if (tableinit===0) tableinit=1;
               algo = "kmeans";
               param = c;
             }

           //Show the chart page
           function chart()
             {
               window.open('chart.jsp', '', 'width=1600, height=1000');
             }

           // delete cached file
           function deleteCache(fnn)
           {
               $.get("./cache.jsp",
                       {
                           fn: fnn,
                           method: "delete"
                       },
                       function (data, status) {
                    	   if (data.includes("true"))
                               $("#md5").html("Deleted");
                           else
                               $("#md5").html("Not deleted");                       });
           }
           
           function updateCache(rq){
               // data cache
               $.get("./cache.jsp",
                       {
                           q: rq,
                           method: "cache"
                       },
                       function (data, status) {
                    	   var res = data.split(";");
                           if (data.includes("true"))
                        	   	{
                                 $("#md5").html( "<button onclick=\"deleteCache('" + res[1] + "')\"> Delete </button> cached file "+res[1]);
                                 cached = true;
                        	   	}
                           else
                        	   {
                        	    $("#md5").html("Not cached");
                        	    cached = false;
                        	   }
                               
                       });
           }
           
           //Prepare geometry data and call the visualization page
           $(document).ready(function () {

           window.name = "myMainWindow";
           //Prepare the dialog window
           $(function () {
                $("#dialog").dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                        "OK": function () {
                            $("#dialog").dialog("close");
                        },
                        Cancel: function () {
                            $("#dialog").dialog("close");
                        }
                    }
                });
           })

           //$("#kmean").hide();
           // Open dump page
           $("#dump").click(function () {
           	console.log("rdfdump");
               window.open('dump.jsp?q=' + rq, '', 'width=1000, height=800');
            });
           // Open the browser page
           $("#browse").click(function () {
                window.open("Browser.jsp?dir=" + $("#path").val(), 'browser', 'width=800,height=600,toolbar=0,menubar=0,location=0');
           });

           //Open the store page
           $("#store").click(function () {
               window.open("store.jsp", 'browser', 'width=1000,height=600,toolbar=0,menubar=0,location=0');
           });

           // Send data to the store to update
           $("#update").click(function () {
              var link = $("#endpoint").val() + "/Update?query=" + encodeURIComponent($("#text").val());
              //  alert(link);
              $.get(link,
                function (data, status) {
                  if (data.toString().includes("true"))
                    alert("Update OK!");
                  else
                    alert("Update failed!");
                  });
           });

           // Send query to the SaveQuery page to save it
           $("#save").click(function () {
             var fname = prompt("File name:", "");
               $.post("SaveQuery.jsp",
                 {
                   fn: fname,
                   text: $("#text").val()
                 },
                 function (data, status) {
                   alert(data);
                 });
           });

           // Send query to the Config page to save it
           $("#config").click(function () {
             window.open("Config.jsp", 'browser', 'width=1000,height=600,toolbar=0,menubar=0,location=0');
           });

           
           // Show options when changing algo 
           $("#algo").change(function (event)
             {
               //  alert($(this).val()); 
               $('#kmean,#part,#apriori,#jrip').hide();

                // alert($(this).val());
               switch ($(this).val() * 1)
                 {
                    case 1:
                        $("#apriori").show();
                        $("#div_variable").show();
                        break;
                    case 2:
                        $("#kmean").show();
                        $("#div_variable").show();
                        break;
                    case 3:
                        $("#part").show();
                        $("#div_variable").show();
                        break;
                    case 3:
                        $("#jrip").show();
                        $("#div_variable").show();
                        break;
                 }
           });

           //Show a dialog for feature selection   
           $("#variable").click(function (event)
           {
             list = va;
            	 //listVariables($("#text").val());
             s = "<select id='listvariable' multiple='multiple' size='10'>";
             for (j = 0; j < list.length; j++)
               // if(va2[j])
               s += "<option value='" + (j + 1) + "' selected='selected'>" + list[j] + "</option>";
               s += "</select>";
               // Fill the dialog and display it
               $("#dialog").html(s);
               $("#dialog").dialog("open");
           });
           
           // bouton CSV
           $("#download-csv").click(function () {
               console.log("download csv ");
             $("#div_list").tabulator("download", "csv", "data.csv");
            });
           
           // bouton JSON
           $("#download-json").click(function () {
               console.log("download json ");
               $("#div_list").tabulator("download", "json", "data.json");
             });
           
           // bouton CHART           
           $("#btn_chart").click(function () {
             console.log("Chart");
             window.open('chart.jsp?q=' + rq, '', 'width=1600, height=1000');
           });

           //bouton MAP
           $("#btn_map").click(function (event) {
		     console.log("Map");
		     window.open('map.jsp?algo=' + algo + '&param=' + param + '&q=' + encodeURIComponent(rq), '', 'width=1500, height=700');
           });
           
           // Start-RUN button click handle
           $("#start").click(function (event) {
		     console.log("start");
             variable = "";
             $('#listvariable :selected').each(function (i, selected) {
               variable += ($(selected).val()) + "-";
             });
             
           q = yasqe.getValue();
           // perhaps pre-processing
           rq = q;
		   // before escape
		   console.log("requete "+rq);
           //console.log("requete "+encodeURIComponent(rq));
           
           rq = encodeURIComponent(rq.replace(/\n/g, " ").replace(/  /g, " ")).trim();
           
           // Choose algorithm
           // apriori
           switch ($("#algo").val() * 1.0)
             {
               case 1:
                 link = 'apriori.jsp?q=' + rq + '&supp=' + $("#supp").val() + "&conf=" + $("#conf").val() + "&var=" + variable;
                 break;
               case 2:
                 link = 'part.jsp?q=' + rq + '&num=' + $("#numPart").val() + '&conf=' + $("#confPart").val() + "&var=" + variable;
                 break;
               case 3:
                 link = 'kmeans.jsp?q=' + rq + '&num=' + $("#num").val() + "&var=" + variable;
                 break;
               case 4:
                 link = 'jrip.jsp?q=' + rq + '&num=' + $("#numjrip").val() + "&var=" + variable;
                 break;
               case 0:
                 link = 'list.jsp?algo=none&param=0&f=json&q=' + encodeURIComponent(rq) + "&var=" + variable;
                 break;
             }
		       console.log("link "+link);
		       
           // data variables 
           $.get("./list.jsp",
                   {
                       q: rq,
                       algo: "none",
                       variables: "true",
                       f: "json"
                   },
                   function (data, status) {
                	   console.log("get list return");
                	   console.log(data);
                	   va = data.split(" ");
                	   console.log("variables - 2 ");
                	   console.log(va);
                	   console.log(status);
                	   // update cache
                	   updateCache(rq);
        		       // algo - on loaded data
                       if ($("#algo").val() * 1.0 > 0){
                         $('#result').load(link);
                         //load data and show Tabulator
                         } 
                       else // perhaps first load and load data
                         {
                           makeTable(tableinit,link);
        		 		   if (tableinit===0) tableinit=1;
                         } 
                    });
  
           
		   console.log("end data variable ");


		       
   
           }
         );
       });


       </script>

   
       <div id="container">
         <!-- End Zone pour la barre en haut -->  
         <!-- End area header -->    
         <header>
           <div id="title"> STRDFMining</div>
         </header>

         <article>
           <div id="dialog" title="Attribute selection">
           <select id='listvariable' multiple='multiple' size='10'>
             <option value='0'> </option>
             <option value='1'>cn1 </option>
             <option value='2'>cn2</option>
           </select>
           </div>

           <div id="query">
             <button class="ui-button ui-widget ui-corner-all" id="config">   Config  </button>
             <button class="ui-button ui-widget ui-corner-all" id="save">   Save  </button>  
             <button class="ui-button ui-widget ui-corner-all" id="browse">   Saved Queries  </button>
             <button class="ui-button ui-widget ui-corner-all" id="update">   Update  </button>
             <button class="ui-button ui-widget ui-corner-all" id="store">   Store  </button>
             <button class="ui-button ui-widget ui-corner-all" id="dump">   RDF Dump  </button> 
             <br>
               <textarea class="textarea"  id="text"></textarea>
               <script>
                 // Sparql Editor
                 var yasqe = YASQE.fromTextArea(document.getElementById('text'));
                 var s = "SELECT *"
                   + "\n WHERE "
                   + "\n {"
                   + "\n  ?s ?p ?o"
                   + "\n }"
                   + "\n LIMIT 10";
                 yasqe.setValue(s);
                 var q;
                 //= yasqe.getValue();
                 var url = location.href; // or window.location.href for current url
                 //  var captured = /myParam=([^&]+)/.exec(url)[1]; // Value is in [1] ('384' in our case)
                 //var q = location.search.split('q')[1]
                 //var s = " PREFIX strdf: <http://strdf.di.uoa.gr/ontology#>"
                 //        + "\n PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
                 //        + "\n PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
                 //        + "\n PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
                 //        + "\n PREFIX time: <http://www.w3.org/2006/time#>"
                 //        + "\n PREFIX gem: <http://za-geminat.cnrs.fr/Assolement.owl#>"
                 //        + "\n SELECT ?name1 ?name2 ?dt1 ?geom1"
                 //        + "\n WHERE "
                 //        + "\n {"
                 //        + "\n  ?ts1 rdf:type gem:TimeSlice."
                 //        + "\n  ?ts1 gem:hasFiliation ?ts2."
                 //        + "\n  ?ts1 gem:hasLandUse ?lu1."
                 //        + "\n  ?ts2 gem:hasLandUse ?lu2."
                 //        + "\n  ?lu1 gem:lname ?name1."
                 //        + "\n  ?lu2 gem:lname ?name2."
                 //        + "\n  ?ts1 gem:pgeometry ?geom1."
                 //        + "\n  ?ts1 gem:hasTime ?t1."
                 //        + "\n  ?t1 time:hasBeginning ?bg1."
                 //        + "\n  ?bg1 time:inXSDDateTime ?dt1."
                 //        + "\n }"
                 //        + "\n LIMIT 1000";
                
                   
                 //if (q === undefined)
                 //  {
                 //    $("#text").html(s);
                 //  } else
                 //  {
                	 //before escape
                 //    var q1 = encodeURIComponent(q);
                 //    q1 = q1.replace(/\+/g, ' ');
                 //    q1 = q1.substr(1);
                 //    $("#text").text(q1);
                 //  }

               </script>
                 <div id="al">
                   <select id="algo">
                     <option value='0' selected='selected'> None</option>
                     <option value='1' > Associator - Apriori</option>
                     <option value='2'> Classifier - PART</option>
                     <option value='4'> Classifier - JRip</option>
                     <option value='3'> Clusterer - Kmeans</option>
                   </select>
                 </div>
                 <div id="param">
                   <div id="apriori">          
                     Supp: <input type="text" id='supp' value='0.01' width="15px">
                     Conf: <input type='text' id='conf' value='0.5'> 
                   </div>
                   <div id="kmean">       
                     Num_clusters: <input type="text" id='num' value='10' width="15px">
                   </div>
                   <div id="part">           
                     Conf: <input type="text" id='confPart' value='0.25' width="15px">
                     Min_Objs: <input type='text' id='numPart' value='2'>     
                   </div>
                   <div id="jrip">           
                     Min_Objs: <input type='text' id='numjrip' value='2'>     
                   </div>
                 </div>
                 <div id="div_variable">
                   <button class="ui-button ui-widget ui-corner-all" id="variable"> Variables </button> 
                 </div>  
                 <div id="buttons">
                   <button class="ui-button ui-widget ui-corner-all" id="start"> Run </button>
                 </div>

           </div><!-- End Zone for Query--> 
           <!-- Zone pour le resultat de fouille--> 
           <!-- Mining Area --> 
           
           <div id="result" class="result"></div>

           <!-- End Zone pour la carte --> 

           <!-- Zone pour la liste en bas -->
           <!-- result Liste Area -->    
              
		
           <div id="listcontainer">   
             <div id="listmenu">
               <div id="listmenu1">   
                 <button class="ui-button ui-widget ui-corner-all" id="download-csv">Download CSV</button>
                 <button class="ui-button ui-widget ui-corner-all" id="download-json">Download JSON</button>
               </div>
               <div id="listmenu2">
                 <button class="ui-button ui-widget ui-corner-all" id="btn_map">Map</button>
                 <button class="ui-button ui-widget ui-corner-all" id="btn_chart">Chart</button>
               </div>
             </div>
             <div id="div_list">
             </div> 

           </div>  

         </article>    

         <footer>
           <!-- <button id="hide"> ✖ Query</button> -->
           <div id="md5"> &nbsp;</div>
           <div id="number">
             <a href="http://l3i.univ-larochelle.fr/">L3i</a>-<a href="https://www.univ-larochelle.fr/">La Rochelle University</a>
           </div>
         </footer>
         <!-- Zone pour la barre en bas-->
         <!-- Footer Area --> 
         
       </div>  

       <input type="hidden" id="data">
       <%
            Config.setJSPPath(request.getRealPath("/"));
            Config.getEndpointURL();
            
            String dir = Config.getJSPPath()+"KB/Query";
            out.write("<input type='hidden' id='endpoint' value='" + Config.endpoint + "'>");
            out.write("<input type='hidden' id='path' value='" + dir + "'>");
	        //out.write("fin");
       %>
    </body>
</html>



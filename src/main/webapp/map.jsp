<%-- 
    Document   : visual
    Created on : Nov 29, 2015, 8:50:03 PM
    Author     : Ba Huy Tran, Alain Bouju
--%>

<%@page import="org.apache.commons.lang.StringEscapeUtils"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="https://openlayers.org/en/v5.3.0/css/ol.css" type="text/css">
        <script src="https://openlayers.org/en/v5.3.0/build/ol.js" type="text/javascript"></script>
        <script type="text/javascript" src="tabulator.js"></script>  
        <link href="tabulator.css" rel="stylesheet">
        <link href="RDFMining.css" rel="stylesheet">
         <style>
      .ol-popup {
        position: absolute;
        background-color: white;
        -webkit-filter: drop-shadow(0 1px 4px rgba(0,0,0,0.2));
        filter: drop-shadow(0 1px 4px rgba(0,0,0,0.2));
        padding: 15px;
        border-radius: 10px;
        border: 1px solid #cccccc;
        bottom: 12px;
        left: -50px;
        min-width: 280px;
      }
      .ol-popup:after, .ol-popup:before {
        top: 100%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
      }
      .ol-popup:after {
        border-top-color: white;
        border-width: 10px;
        left: 48px;
        margin-left: -10px;
      }
      .ol-popup:before {
        border-top-color: #cccccc;
        border-width: 11px;
        left: 48px;
        margin-left: -11px;
      }
      .ol-popup-closer {
        text-decoration: none;
        position: absolute;
        top: 2px;
        right: 8px;
      }
      .ol-popup-closer:after {
        content: "✖";
      }
    </style>
    </head>
    <body>
        <div id="container">
            <!-- End Zone pour la barre en haut -->    
            <header>
                <div id="title"> Map </div>
            </header>
            <article>

                <div id="map" class="map"></div>  
                <div id="popup" class="ol-popup">
                    <a href="#" id="popup-closer" class="ol-popup-closer"></a>
                    <div id="popup-content"></div>
                    <div id="traj"></div>
                </div> 
            </article>
            <footer>
                <a href="http://l3i.univ-larochelle.fr/">L3i</a>-<a href="https://www.univ-larochelle.fr/">La Rochelle University</a>
                <div id="number"></div>
            </footer>
            <!-- Zone pour la barre en bas--> 
            <script>

                var raster = new ol.layer.Tile({
                    source: new ol.source.OSM()
                });



                var format = new ol.format.WKT();
                var features = [];
                var vectorSource = new ol.source.Vector({
                    features: features      //add an array of features
                            //,style: iconStyle     //to set the style for all your features...
                });

                var vectorLayer = new ol.layer.Vector({
                    source: vectorSource
                });

                function listVariables(q)
                {
                    var va = q.substring(q.indexOf("select"), q.indexOf("{"));
                    v = q.replace(/\./g, '');
                    var re = /\?\w+\s*\.*/g;
                    //  alert(va);
                    var variable;

                    // If select all, find all variables
                    if (va.includes("*"))
                        variable = v.match(re);
                    else
                        variable = va.match(re);

                    for (j = 0; j < variable.length; j++)
                        variable[j] = variable[j].trim().replace("?", "");
                    var unique = variable.filter(function (item, i, ar) {
                        return ar.indexOf(item) === i;
                    });
                    // alert(variable);
                    return unique;
                }
                // Find selected variables otherwise

                function getParameterByName(name) {
                    url = window.location.href;
                    name = name.replace(/[\[\]]/g, "\\$&");
                    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                            results = regex.exec(url);
                    if (!results)
                        return null;
                    if (!results[2])
                        return '';
                    console.log(results[2].replace(/\+/g, " "));
                    return decodeURIComponent(results[2].replace(/\+/g, " "));
                }


                var list = listVariables(unescape(getParameterByName('q')).toLowerCase());

                var color = ["#0e2431", "#415b90", "#c4aff0", "#65799b"];

                function pointStyle(index) {

                    var style = new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 4,
                            fill: new ol.style.Fill({
                                color: color[index]
                            })

                        })

                    });
                            return [style];
                }

                function polygonStyle(index) {
                    var style = new ol.style.Style({

                        stroke: new ol.style.Stroke({
                            color: color[3 - index],
                            width: 1

                        })


                    });
                    return [style];
                }

                $.ajax({
                    type: 'GET',
                    url: location.href.replace("map.jsp?", "list.jsp?f=json&"),
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (index, item) {
                            var poly = 0;
                            var point = 0;
                            var name = "";
                            for (i = 0; i < list.length; i++)
                            {

                                if (list[i].startsWith("geom"))
                                {
                                    var geom = eval("item." + list[i]);
                                    if (geom.includes("MULTIPOLYGON"))
                                    {
                                        geom = geom.replace("MULTIPOLYGON", "MULTILINESTRING");
                                        geom = geom.replace("(((", "((");
                                        geom = geom.replace(")))", "))");
                                        poly++;
                                    } else
                                    if (geom.includes("POLYGON"))
                                    {
                                        geom = geom.replace("POLYGON", "LINESTRING");
                                        geom = geom.replace("((", "(");
                                        geom = geom.replace("))", ")");
                                        poly++;
                                    }
                                    if (geom.includes("POINT"))
                                    {
                                        point++;
                                    }
									//EPSG:3857 OpenStreetMap - WGS84  EPSG:4326 - WGS84
                                    var f = format.readFeature(geom, {
                                        dataProjection: 'EPSG:4326',
                                        featureProjection: 'EPSG:3857'
                                    });
                                    // alert(index);
                                    f.set("name", name);
                                    if(point>1 || poly>1)
                                    if (geom.includes("POINT"))
                                        f.setStyle(pointStyle(point - 1));
                                    else
                                        f.setStyle(polygonStyle(poly - 1));
                                    vectorSource.addFeature(f);
                                    //name="";
                                } else
                                {
                                 var propList = "";
                                 for(var propName in item) {
                                    if ((typeof(item[propName]) != "undefined")&&(!propName.includes("geom"))) {
                                      propList += "<b>"+(propName + ":</b> ");
                                      if (item[propName].startsWith("http"))
                                	  {
                                      propList += ("<a href=RDFD.jsp?query="+encodeURIComponent(item[propName])+">"+item[propName]+"</a><br>");
                                	  }//if                           
                                      else
                                	  {
                                      propList += (item[propName]+"<br>");
                                	  }//else
                                	 }//if
                                    }//for 
                                 name=propList;
                                 //console.log("name "+name+ " i "+i+ " item "+item+" list[i]"+list[i]+" eval "+eval("item." + list[i])+ " propList "+propList+" "+item[list[i]]);
                                }//else

                            }//for
						map.getView().fit(vectorSource.getExtent(), map.getSize());
                        });
                    }
                    });//ajax

                    // Afficher les coordonnées en function de la position du curseur
                    var mousePositionControl = new ol.control.MousePosition({
                    coordinateFormat: ol.coordinate.createStringXY(4),
                    projection: 'EPSG:4326',
                    className: 'custom-mouse-position',
                    target: document.getElementById('mouse-position'),
                    undefinedHTML: ''
                });

                var hiddenStyle = new ol.style.Style({
                    image: new ol.style.RegularShape({}) //a shape with no points is invisible
                });

                // Définition de la carte
                map = new ol.Map({
                    controls: ol.control.defaults({
                        attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                            collapsible: false
                        })
                    }).extend([mousePositionControl]),
                    target: 'map',
                    layers: [
                        new ol.layer.Tile({
                            source: new ol.source.OSM()
                        })
                    ],
                    view: new ol.View({
                    	// Chize -0.33, 46.3 La Rochelle -1.1517,46.1591
                    	// EPSG:3857 OpenStreetMap, Google ...
                        center: ol.proj.transform([-1.1517,46.1591], 'EPSG:4326', 'EPSG:3857'),
                        zoom: 13
                    }),
                });

                map.addLayer(vectorLayer);
                // Chagement du style du curseur quand il tombe sur un objet
                var cursorHoverStyle = "pointer";
                var target = map.getTarget();
                var jTarget = typeof target === "string" ? $("#" + target) : $(target);
                map.on("pointermove", function (evt) {
                    //  var mouseCoordInMapPixels = [event.originalEvent.offsetX, event.originalEvent.offsetY];

                    //detect feature at mouse coords
                    var hit = map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {
                        //alert(feature.get('name'));
                        return true;
                    });

                    if (hit) {
                        jTarget.css("cursor", cursorHoverStyle);
                    } else {
                        jTarget.css("cursor", "");
                    }
                });

                // Affichage des infos d'un objet 
                var container = document.getElementById('popup');
                var content = document.getElementById('popup-content');
                var closer = document.getElementById('popup-closer');
                var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
                    element: container,
                    autoPan: true,
                    autoPanAnimation: {
                        duration: 250
                    }
                }));
                map.addOverlay(overlay);
                /**
                 * Add a click handler to hide the popup.
                 * @return {boolean} Don't follow the href.
                 */
                closer.onclick = function () {
                    overlay.setPosition(undefined);
                    closer.blur();
                    return false;
                };


                map.on('singleclick', function (evt) {
                    // alert("ok");

                    map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {

                        content.innerHTML = feature.get("name");
                        overlay.setPosition(evt.coordinate);
                    });
                    // var coordinate = evt.coordinate;

                });
            </script>
    </body>
</html>

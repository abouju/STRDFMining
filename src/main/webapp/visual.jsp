<%-- 
    Document   : visual
    Created on : Nov 29, 2015, 8:50:03 PM
    Author     : Ba Huy Tran, Alain Bouju
--%>

<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="ol.js"></script>
        <title>RDF Mining</title>
        <link rel="stylesheet" href="nouislider.css" type="text/css">
    </head>
    <body>
        <script src="script/jquery.js"></script>
        <link rel="stylesheet" href="css/ol.css" type="text/css">
        <script src="script/ol.js"></script>

    </head>
<body>

    <div class="container-fluid">

        <div class="row-fluid">
            <div class="span12">
                <div id="map" class="map"></div>
            </div>
        </div>

    </div>
    <script>
        var blue = [0, 153, 255, 1];
        var red = [0, 176, 23, 31];
        var white = [255, 255, 255, 1];
        var width = 2;

        var parent = $(window.opener.document).contents();
        var raster = new ol.layer.Tile({
            source: new ol.source.OSM()
        });

        var polySet1 = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({color: '#666600', width: 1, strokeOpacity: 0.5}),
                fill: new ol.style.Fill({color: 'rgba(255,170,100,0.1)'})
            })

        });

        var polySet2 = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({color: '#003366', wbbidth: 1, strokeOpacity: 1, lineDash: [40, 40]}),
                fill: new ol.style.Fill({color: 'rgba(153,255,255 ,0.1)'})
            })

        });

        var pointSet1 = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                image: new ol.style.Circle({
                    radius: width * 2,
                    fill: new ol.style.Fill({
                        color: blue
                    }),
                    stroke: new ol.style.Stroke({
                        color: white,
                        width: width / 2
                    })
                }),
                zIndex: Infinity
            })

        });

        var pointSet2 = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                image: new ol.style.Circle({
                    radius: width * 2,
                    fill: new ol.style.Fill({
                        color: red
                    }),
                    stroke: new ol.style.Stroke({
                        color: white,
                        width: width / 2
                    })
                }),
                zIndex: Infinity
            })

        });

        var format = new ol.format.WKT();
        var features = [];
    //polygone set 1
        listgeom = parent.find("#poly1").val();
        var lines = listgeom.split('\n');
    //read polySet1

        $.each(lines, function (key, line) {
            if (line !== "")
            {
                feature = new ol.Feature({geometry: format.readFeature(line).getGeometry().transform("EPSG:4326", "EPSG:900913")});
                feature.attributes = {name: "ok"};
                features.push(feature);
            }
        });
        polySet1.getSource().addFeatures(features);

    //read polySet2
        listgeom = parent.find("#poly2").val();
        var features = [];
        var lines = listgeom.split('\n');
        $.each(lines, function (key, line) {
            if (line !== "")
            {
                // alert(line);   
                feature = new ol.Feature({geometry: format.readFeature(line).getGeometry().transform("EPSG:4326", "EPSG:900913")});
                features.push(feature);
            }
        });
        polySet2.getSource().addFeatures(features);


        //read pointSet1
        listpoint = parent.find("#point1").val();
        lines = listpoint.split('\n');
        features = [];
        $.each(lines, function (key, line) {
            if (line !== "")
            {
                feature = new ol.Feature({geometry: format.readFeature(line).getGeometry().transform("EPSG:4326", "EPSG:900913")});
                features.push(feature);
            }

        });


        pointSet1.getSource().addFeatures(features);

        //
        //read pointSet2
        listpoint2 = parent.find("#point2").val();
        lines2 = listpoint2.split('\n');
        features2 = [];
        $.each(lines2, function (key, line2) {
            if (line2 !== "")
            {
                feature2 = new ol.Feature({geometry: format.readFeature(line2).getGeometry().transform("EPSG:4326", "EPSG:900913")});
                features2.push(feature2);
            }
        });
        pointSet2.getSource().addFeatures(features2);

        var map = new ol.Map({
            layers: [raster],
            target: 'map',
            view: new ol.View({
                center: ol.proj.transform([-0.33, 46.3], 'EPSG:4326', 'EPSG:900913'),
                zoom: 13
            })
        });
        var mousePosition = new ol.control.MousePosition({
            coordinateFormat: ol.coordinate.createStringXY(3),
            projection: 'EPSG:4326'
        });

        map.addControl(mousePosition);
        map.addLayer(pointSet1);
        map.addLayer(pointSet2);
        map.addLayer(polySet1);
        map.addLayer(polySet2);

    </script>

    <div id="map" class="smallmap"></div>
</body>
</html>

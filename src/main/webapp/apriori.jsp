<%-- 
    Document   : apriori
    Created on : Nov 25, 2015, 5:18:02 PM
    Author     : Ba Huy Tran, Alain Bouju
--%>


<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.File"%>
<%@page import="ulr.l3i.strdfmining.AprioriWeb"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="ulr.l3i.strdfmining.QueryProcessing"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ulr.l3i.strdfmining.Config"%>

<%
	// Config endpoint
    Config.setJSPPath(request.getRealPath("/"));
    Config.getEndpointURL();
    
    String supp = "";
    String conf = "";
    String variable = "";
	String req = java.net.URLDecoder.decode(((String[])request.getParameterMap().get("q"))[0], "ISO-8859-1");

    if (!req.equals("")) {
        supp = request.getParameter("supp");
        conf = request.getParameter("conf");
        variable = request.getParameter("var");
        //out.print(req);
        

        String result = QueryProcessing.getMD5(req + "csv");
        String fn = request.getRealPath("/")+"/KB/Cache/" + result;
        File file = new File(fn);

        if (!file.exists()) {
            String resp = "";

            resp = QueryProcessing.queryCSVQ(req);

            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(resp);
            bw.close();
        }

        AprioriWeb.RunApriori(fn, variable, Float.parseFloat(supp), Float.parseFloat(conf));
        out.print(AprioriWeb.listRules());
    } else {
        out.print(AprioriWeb.getRuleInstancesJSON(Integer.parseInt(request.getParameter("r"))));
    }


%>

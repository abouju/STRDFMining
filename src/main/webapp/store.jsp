<%-- 
    Document   : store
    Created on : Nov 12, 2016, 11:18:46 AM
    Author     : Ba Huy Tran, Alain Bouju
--%>
<!-- 
Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à [rappeler les
caractéristiques techniques de votre logiciel]. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
 -->
 
<%@page import="ulr.l3i.strdfmining.Config"%>
<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Store Page</title>
    <script src="script/jquery.js"></script>
    <script>
      // Set cursor
      $(document).ajaxStart(function() {
      $(document.body).css({'cursor' : 'wait'});
        }).ajaxStop(function() {
        $(document.body).css({'cursor' : 'default'});
        });

    </script>
  </head> 
  <body bgcolor="#FFFFF0">
  <%
            File dir = new File(getServletContext().getRealPath("/").replace('\\', '/')+"KB/Store");
           
            File[] list = dir.listFiles();
            String s="";
            for(int c=0;c<list.length;c++)
            {
                if(list[c].isFile())
                    if(list[c].getName().indexOf("jsp")<0)
                s+="<option value='"+list[c].getAbsolutePath().replace('\\', '/')+"'>"+ list[c].getName()+"</option>";
            }
            
             out.write("<input type='hidden' id='endpoint' value='"+Config.endpoint+"'>");
        %>
        <FORM method="POST" name="form" enctype="UTF-8" accept-charset="UTF-8">
        <INPUT type=hidden name="view" value=""/>
         <INPUT type=hidden name="graph" value=""/>
        <TABLE cellspacing="5">
	<tr>
		<td colspan=2 >
			<div style="font-size:13px"> 
				You must be logged in to store, or run in localhost.
			</div>
		</td>	
	</tr>
	<tr>
		<td >RDF Format:</td>
		<td >
				<SELECT name="format" title="select one of the following RDF graph format types">
				
					<OPTION value="RDF/XML">RDF/XML</OPTION>
				
					<OPTION value="N-Triples">N-Triples</OPTION>
				
					<OPTION value="Turtle">Turtle</OPTION>
				
					<OPTION value="N3">N3</OPTION>
				
					<OPTION value="TriX">TriX</OPTION>
				
					<OPTION value="TriG">TriG</OPTION>
				
					<OPTION value="BinaryRDF">BinaryRDF</OPTION>
				
				</SELECT>
		</td>
	</tr>
		<tr>
		<td >Inference:</td>
		<td >
				<input type="checkbox" title="Enable Inference" name="inference" value="true"> <br>
		</td>
	</tr>
	<tr>
		<!--  direct input form -->
		<td >Direct Input:</td>
		<td >
			<textarea id="data" name="data" rows="15" cols="100"></textarea></td>
	</tr>
	<tr>
		<td colspan=2 ><br/>
		<CENTER>
			<input type="button" id="direct" value="Store Input" name="dsubmit" style="width: 350px"/>
		</CENTER><br/>
		</td>
	</tr>
	
	<tr>
		<td >URI Input:</td>
		<td >
			<input type='hidden' id='url' name="url"> 
                        <select id='file'>
                        <%= s %>
                        </select>
		</td>
	</tr>
	
	<tr>
		<td colspan=2 ><br/>
                  
			<CENTER>
				<INPUT id="uri" type="button" value="Store from URI" name="fromurl" style="width: 350px"/>
			</CENTER><br/>
		</td>
	</tr>
	
	</TABLE>
        </form>  
        <script>
            var link=$("#endpoint").val()+"/StoreMod";
            
            
            $("#uri").click(function() {
            var v="file://"+$("#file option:selected").val();
          
            $("#url").val(v);
            $("#data").val("");
            $.ajax({
               data: $("form").serialize(),
               type: 'POST',
               url: link,
               success: function(data, status){
                    if(status.toString().includes("success"))
                        alert("Update OK!");
                    else
                         alert("Update failed!");
                     
                   }
                  });
               
                });
           
            $("#direct").click(function() {
                  
            $("#url").val("");
             $.ajax({
               data: $("form").serialize(),
               type: 'POST',
               url: link,
               success: function(data, status){
                //   alert(data);
                //   alert(status);
                    if(status.toString().includes("success"))
                        alert("Update OK!");
                    else
                         alert("Update failed!");
                     
                   }
                  });             
                });
            </script>
  </body>
</html>

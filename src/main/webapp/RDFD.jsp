<%-- 
    Document   : RDFData
    Created on : Nov 7, 2016, 11:08:22 AM
    Author     : Ba Huy Tran, Alain Bouju
    
Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à "STRDFMining". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to STRDFMining (Spatio-Temporal RDF Mining).

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ulr.l3i.strdfmining.QueryProcessing"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLDecoder"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RDF Data</title>
        <script src="script/jquery.js"></script>
        <style>
            .list td, .list th
            {
                max-width:500px;
                min-width:30px;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap; 
                border-bottom: 1px solid #ddd;
                border: 1px solid #ddd;
                padding: 8px;
                cursor: pointer;
                cursor: hand;
            }


            .list tr:nth-child(even){background-color: #f2f2f2;}

            .list td:hover {background-color: #ddd;}
            .list th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }
            div
            {
                display: inline-block; 
            }
            .list
            {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }
        </style>

    </head>
    <body>

        <%

            String req = URLDecoder.decode(request.getParameter("query"));

            //If it's a point or polygon, send to the viz page
            if (req.lastIndexOf("POLY") >= 0 || req.lastIndexOf("POINT") >= 0) {
                response.sendRedirect("visual2.jsp?query=" + request.getParameter("query"));
            } //show data table orthewise
            else {
                String q1 = "";
                if (req.indexOf("http") >= 0) {
                    q1 = "select * where { ?subject ?predicate ?object . FILTER((?subject = <" + req + ">)  || (?object = <" + req + ">)) }";
                } else {
                    q1 = "select * where { ?subject ?predicate ?object . FILTER((?object = \"" + req + "\")) }";
                }
                String s[] = QueryProcessing.queryCSV(q1).split("\n");

                out.write("<table id='list' class='list'>");
                out.write("<tr><th>" + s[0].split(",")[0] + "</th><th>" + s[0].split(",")[1] + "</th><th>" + s[0].split(",")[2]+ "</th></tr>");

                for (int i = 1; i < s.length; i++) {
                    String ss[] = s[i].split(",");
                    out.write("<tr>");
                    if (ss.length > 3) {
                        for (int k = 3; k < ss.length; k++) {
                            ss[2] = ss[2] + ", " + ss[k];
                        }
                        ss[2] = ss[2].replaceAll("\"", "");
                    }
                    for (int j = 0; j < 3; j++) // if(ss[j].trim().equals(req.trim()) || (j==1))
                    {
                        out.write("<td>" + ss[j].trim() + "</td>");
                    }
                    // else
                    //  out.write("<td><a href=''>"+ss[j].trim()+"</a></td>");
                    out.write("</tr>");

                }
                out.write("</table>");
            }
        %>     

        <script>
            // Handle data table click
            $('#list td').click(function (event) {
                var query = $(this).text();
                //  var query="select * where { ?subject ?predicate ?object . FILTER((?subject = "+e+") || (?predicate = "+e+")  || (?object = "+e+")) }";
                window.open("RDFD.jsp?query=" + encodeURIComponent(query), '', 'height=800,width=1500');
                event.stopPropagation();
                event.preventDefault();
            });
        </script>     
    </body>
</html>

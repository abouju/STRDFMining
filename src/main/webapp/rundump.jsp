<%-- 
    Document   : rundump
    Created on : Nov 25, 2015, 5:18:02 PM
    Author     : Ba Huy Tran, Alain Bouju
--%>
<!-- 
Copyright 

Laboratoire L3i
Universit� de La Rochelle
Avenue Michel Cr�peau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant � [rappeler les
caract�ristiques techniques de votre logiciel]. 

Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilit� au code source et des droits de copie,
de modification et de redistribution accord�s par cette licence, il n'est
offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les conc�dants successifs.

A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement,  � l'utilisation,  � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
avertis poss�dant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
logiciel � leurs besoins dans des conditions permettant d'assurer la
s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
� l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accept� les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Cr�peau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
 -->
<%@page import="java.io.FileReader"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%
    String fn = request.getParameter("fn");
    String view = request.getParameter("view");
    String mapping = request.getParameter("mapping");
    String file = getServletContext().getRealPath("") + "/" + Math.abs(mapping.hashCode()) + ".ttl";
    String dest = getServletContext().getRealPath("") + "/KB/Store/" + fn;
    FileWriter filewriter = new FileWriter(file);
    filewriter.write(mapping);
    filewriter.close();
    String command = getServletContext().getRealPath("") + "/d2rq/dump-rdf -out " + dest + " " + file;

    try {
        Process p = Runtime.getRuntime().exec(command);
        p.waitFor();
        String line = "";
        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = input.readLine()) != null) {
            out.write(line);
        }
        input.close();

        BufferedReader errorReader = new BufferedReader(
                new InputStreamReader(p.getErrorStream()));
        while ((line = errorReader.readLine()) != null) {
            out.write(line);
        }

        errorReader.close();

    } catch (Exception e) {
        out.write(e.getMessage());
    }
    File f = new File(dest);
    if (f.exists()) {
        if (view.equals("true")) {
            BufferedReader reader = new BufferedReader(new FileReader(dest));
            //BufferedReader br = new InputStreamReader(new FileInputStream(txtFilePath));
            StringBuilder sb = new StringBuilder();
            String line;

            while((line = reader.readLine())!= null){
                sb.append(line+"\n");
            }
            out.println(sb.toString()); 
        }
        else
               out.write("OK");
    }
%>

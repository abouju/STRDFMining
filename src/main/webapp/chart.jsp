<%-- 
    Document   : chart
    Created on : Nov 29, 2015, 8:50:03 PM
    Author     : Ba Huy Tran, Alain Bouju


Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Ba-Huy Tran 
Alain Bouju

Ce logiciel est un programme informatique servant à [rappeler les
caractéristiques techniques de votre logiciel]. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Ba-Huy Tran 
Alain Bouju

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
--%>
 
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Spatio-temporal RDF Data Mining</title>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <link href="RDFMining.css" rel="stylesheet">
        <script src="script/jquery.js"></script>
        <script type="text/javascript">
            var type = '3';
            var link = location.href.replace("chart.jsp?", "list.jsp?f=json&param=0&algo=none&");
            var header = [];
            $.ajaxSetup({
                async: false
            });
            // If to viz less than 2 columns (combo char is not possible)

            //   if ($('#list', window.opener.document).find("tr:first th").length <= 3)
            type = prompt("Chart type \n1. Column chart \n2. Line chart \n3. Combo chart \n4. Pie chart \n5. Scatter chart", "1");
            google.charts.load('current', {'packages': ['corechart', 'table']});
            // Set a callback to run when the Google Visualization API is loaded.
            if (type === '3')
                google.charts.setOnLoadCallback(drawChart); // combo chart
            else
                google.charts.setOnLoadCallback(drawChart2); //the others

            // draw combo chart 
            function drawChart() {
                var values = []; // to hold our values for data table
                // get our values
                var u0 = [];
                var u1 = [];

                var values = [];
                var j = 0;
                //alert( $('#list',window.opener.document).html());
                $.getJSON(link, function (result) {
                    values[0] = [];
                    $.each(result, function (i, v) {
                        //  alert(i);
                        values[i + 1] = [];
                        j = 0;
                        // select either th or td, doesn't matter
                        $.each(v, function (ii, vv) {

                            if (i === 0)
                                values[0][j] = ii;
                            if (isNaN(vv))
                                values[i + 1][j] = vv;
                            else
                                values[i + 1][j] = vv * 1.0;
                            if (j === 1)
                                if (u0.indexOf(vv) < 0)
                                    u0.push(vv);
                            //unique values of the second column
                            if (j === 2)
                                if (u1.indexOf(vv) < 0)
                                    u1.push(vv);
                            j++;
                        });
                    });
                });

                //Create row header 
                var matrix = [];
                for (i = 0; i < u0.length + 1; i++)
                {
                    matrix[i] = new Array(u1.length + 1);

                    if (i > 0)
                        matrix[i][0] = u0[i - 1];
                }

                //Create column header
                for (i = 0; i < u1.length; i++)
                {
                    matrix[0][i + 1] = u1[i];
                }
                matrix[0][0] = "*";

                // convert 2d array to dataTable
                for (i = 1; i < values.length; i++)
                {
                    matrix[u0.indexOf(values[i][1]) + 1][u1.indexOf(values[i][2]) + 1] = values[i][0];
                    //    alert(values[i][0]+" "+values[i][1]+" "+values[i][2]);
                    //    alert(u0.indexOf(values[i][0]))
                }

                ti = prompt("Graph's title", "Visualization");
                var options = {'title': ti, 'height': 600, seriesType: 'bars', };
                var data = new google.visualization.arrayToDataTable(matrix);
                var table = new google.visualization.Table(document.getElementById('table_div'));
                var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                table.draw(data);
                chart.draw(data, options);
            }

            // draw the others
            function drawChart2() {
                var values = [];
                var j = 0;
                //alert( $('#list',window.opener.document).html());
                $.getJSON(link, function (result) {
                    values[0] = [];
                    $.each(result, function (i, v) {
                        //  alert(i);
                        values[i + 1] = [];
                        j = 0;
                        // select either th or td, doesn't matter
                        $.each(v, function (ii, vv) {

                            if (i === 0)
                                values[0][j] = ii;
                            if (isNaN(vv))
                                values[i + 1][j] = vv;
                            else
                                values[i + 1][j] = vv * 1.0;
                            // alert(values[i][ii]);
                            j++;
                        });
                    });
                });

                var temp;
                for (i = 0; i < values.length; i++) {
                    temp = values[i][0];
                    values[i][0] = values[i][values[i].length - 1];
                    values[i][values[i].length - 1] = temp;
                }

                // convert 2d array to dataTable and draw
                ti = prompt("Please enter the graph's title", "Visualization");
                var options = {'title': ti, 'height': 600};
                var data = new google.visualization.arrayToDataTable(values);
                var table = new google.visualization.Table(document.getElementById('table_div'));
                var chart;
                if (type == '1')
                    chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                else
                if (type == '2')
                    chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                else
                if (type == '4')
                    chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                else
                    chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
                table.draw(data);
                chart.draw(data, options);
            }
        </script>
    </head>
    <body>
        <div id="container">
            <!-- End Zone pour la barre en haut -->    
            <header>
                <div id="title"> Chart </div>
            </header>
            <article>
                <div id="chart_div"></div>
                <div id="table_div"></div> 
            </article>
            <footer>
                <div id="number">
                  <a href="http://l3i.univ-larochelle.fr/">L3i</a>-<a href="https://www.univ-larochelle.fr/">La Rochelle University</a>
                </div>
            </footer>
            <!-- Zone pour la barre en bas--> 
    </body>
</html>
